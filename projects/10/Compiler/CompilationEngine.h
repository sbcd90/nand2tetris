#pragma once
#ifndef COMPILATION_ENGINE_H_
#define COMPILATION_ENGINE_H_

#include "JackTokenizer.h"

class CompilationEngine {
    std::shared_ptr<JackTokenizer> tokenizer;
    int indent;
    std::ofstream output;
    std::vector<std::string> OPS;
protected:
    void writeKeyword();
    void writeIdentifier();
    void writeSymbol();
    void writeIntConstant();
    void writeStringConstant();
    void compileVarDecInt();
public:
    CompilationEngine(const std::string &inputFilePath, const std::string &outputPath);
    void compileClass();
    void compileClassVarDec();
    void compileSubroutineDec();
    void compileParameterList();
    void compileSubroutineBody();
    void compileVarDec();
    void compileStatements();
    void compileLet();
    void compileIf();
    void compileWhile();
    void compileDo();
    void compileReturn();
    void compileExpression();
    void compileTerm();
    void compileExpressionList();
};

#endif // COMPILATION_ENGINE_H_