#pragma once
#ifndef JACK_TOKENIZER_H_
#define JACK_TOKENIZER_H_

#include <regex>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iterator>
#include <string>
#include <algorithm>

enum JackTokens {
    KEYWORD,
    SYMBOL,
    INT_CONST,
    STRING_CONST,
    IDENTIFIER
};

class JackTokenizer {
    const std::regex COMMENT{"(//.*)|(/\\*([^*]|[\r\n]|(\\*+([^*/]|[\r\n])))*\\*+/)"};
    const std::regex EMPTY_TEXT_PATTERN{"\\s*"};
    const std::regex KEYWORD_PATTERN{"^\\s*(class|constructor|function|method|static|field|var|int|char|boolean|void|true|false|null|this|let|do|if|else|while|return)\\s*"};
    const std::regex SYMBOL_PATTERN{"^\\s*([{}()\\[\\].,;+\\-*/&|<>=~])\\s*"};
    const std::regex DIGIT_PATTERN{"^\\s*(\\d+)\\s*"};
    const std::regex STRING_PATTERN{"^\\s*\"(.*)\"\\s*"};
    const std::regex IDENTIFIER_PATTERN{"^\\s*([a-zA-Z_][a-zA-Z1-9_]*)\\s*"};

    std::vector<std::string> keywords;
    std::string text;
    JackTokens tokenTypeVar;
    std::string currentToken;
    std::ifstream inputFile;

    std::vector<std::string> regexSearch(const std::regex& regex);
protected:
    static inline void ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
            return !std::isspace(static_cast<char>(ch), std::locale("en_US.UTF-8"));
        }));
    }

    static inline void rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
            return !std::isspace(static_cast<char>(ch), std::locale("en_US.UTF-8"));
        }).base(), s.end());
    }

    static inline void trim(std::string &s) {
        rtrim(s);
        ltrim(s);
    }

    void clearAllComments();
public:
    JackTokenizer(const std::string& input_file_path);
    bool hasMoreTokens();
    void advance();
    JackTokens tokenType();
    std::string getKeyWord();
    std::string getSymbol();
    std::string getIdentifier();
    std::string getIntVal();
    std::string getStringVal();

    static inline std::vector<std::string> splitString(const std::string &s, const std::string &delimiter = " ") {
        auto splits = std::vector<std::string>{};
        auto start = 0;
        auto end = s.find(delimiter);
        while (end != std::string::npos) {
            splits.push_back(s.substr(start, end - start));
            start = end + delimiter.length();
            end = s.find(delimiter, start);
        }
        splits.push_back(s.substr(start, end - start));
        return splits;
    }

    static inline std::string getLastInstanceOf(const std::string &s, const std::string &delimiter = " ") {
        auto splits = splitString(s, delimiter);
        return splits[splits.size()-1];
    }
};

#endif // JACK_TOKENIZER_H_