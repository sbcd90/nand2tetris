#include "CompilationEngine.h"

CompilationEngine::CompilationEngine(const std::string &inputFilePath, const std::string &outputPath):
tokenizer(std::make_shared<JackTokenizer>(inputFilePath)), output(std::ofstream{outputPath}), indent(0),
OPS(std::vector<std::string>{}) {
    this->OPS.emplace_back("+");
    this->OPS.emplace_back("-");
    this->OPS.emplace_back("*");
    this->OPS.emplace_back("/");
    this->OPS.emplace_back("&amp;");
    this->OPS.emplace_back("|");
    this->OPS.emplace_back("&lt;");
    this->OPS.emplace_back("&gt;");
    this->OPS.emplace_back("=");
}

void CompilationEngine::compileClass() {
    if (this->tokenizer->hasMoreTokens()) {
        tokenizer->advance();
        output <<  "<class>" << std::endl;
        indent += 2;

        writeKeyword();
        tokenizer->advance();
        writeIdentifier();

        tokenizer->advance();
        writeSymbol();

        tokenizer->advance();
        while (tokenizer->getKeyWord() == "static" ||
        tokenizer->getKeyWord() == "field") {
            compileClassVarDec();
        }
        while (tokenizer->getKeyWord() == "constructor" ||
        tokenizer->getKeyWord() == "function" || tokenizer->getKeyWord() == "method") {
            compileSubroutineDec();
        }

        writeSymbol();

        indent -= 2;
        output <<  "</class>" << std::endl;
        output.close();
    }
}

void CompilationEngine::compileClassVarDec() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<classVarDec>" << std::endl;
    indent += 2;
    writeKeyword();

    tokenizer->advance();
    compileVarDecInt();
    indent -= 2;

    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "</classVarDec>" << std::endl;
}

void CompilationEngine::compileVarDec() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<varDec>" << std::endl;
    indent += 2;

    writeKeyword();
    tokenizer->advance();
    compileVarDecInt();

    indent -= 2;
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "</varDec>" << std::endl;
}

void CompilationEngine::compileVarDecInt() {
    if (tokenizer->tokenType() == JackTokens::KEYWORD) {
        writeKeyword();
    } else if (tokenizer->tokenType() == JackTokens::IDENTIFIER) {
        writeIdentifier();
    }
    tokenizer->advance();
    writeIdentifier();
    tokenizer->advance();
    while (tokenizer->getSymbol() == ",") {
        writeSymbol();
        tokenizer->advance();
        writeIdentifier();
        tokenizer->advance();
    }
    writeSymbol();
    tokenizer->advance();
}

void CompilationEngine::compileSubroutineDec() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<subroutineDec>" << std::endl;
    indent += 2;
    writeKeyword();

    tokenizer->advance();
    if (tokenizer->tokenType() == JackTokens::KEYWORD) {
        writeKeyword();
    } else if (tokenizer->tokenType() == JackTokens::IDENTIFIER) {
        writeIdentifier();
    }

    tokenizer->advance();
    writeIdentifier();

    tokenizer->advance();
    writeSymbol();

    tokenizer->advance();
    compileParameterList();

    writeSymbol();

    tokenizer->advance();
    compileSubroutineBody();
    indent -= 2;
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "</subroutineDec>" << std::endl;
    tokenizer->advance();
}

void CompilationEngine::compileSubroutineBody() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<subroutineBody>" << std::endl;
    indent += 2;
    writeSymbol();

    tokenizer->advance();
    while (tokenizer->getKeyWord() == "var") {
        compileVarDec();
    }

    compileStatements();

    writeSymbol();
    indent -= 2;
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "</subroutineBody>" << std::endl;
}

void CompilationEngine::compileStatements() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<statements>" << std::endl;
    indent += 2;
    while (tokenizer->tokenType() == JackTokens::KEYWORD) {
        if (tokenizer->getKeyWord() == "let") {
            compileLet();
        } else if (tokenizer->getKeyWord() == "if") {
            compileIf();
        } else if (tokenizer->getKeyWord() == "while") {
            compileWhile();
        } else if (tokenizer->getKeyWord() == "do") {
            compileDo();
        } else if (tokenizer->getKeyWord() == "return") {
            compileReturn();
        }
    }
    indent -= 2;
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "</statements>" << std::endl;
}

void CompilationEngine::compileReturn() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<returnStatement>" << std::endl;
    indent += 2;
    writeKeyword();

    tokenizer->advance();
    if (tokenizer->tokenType() != JackTokens::SYMBOL &&
    tokenizer->getSymbol() != ";") {
        compileExpression();
    }

    writeSymbol();

    indent -= 2;
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "</returnStatement>" << std::endl;
    tokenizer->advance();
}

void CompilationEngine::compileDo() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<doStatement>" << std::endl;
    indent += 2;

    writeKeyword();

    tokenizer->advance();

    writeIdentifier();
    tokenizer->advance();
    if (tokenizer->getSymbol() == ".") {
        writeSymbol();
        tokenizer->advance();
        writeIdentifier();
        tokenizer->advance();
    }
    writeSymbol();

    tokenizer->advance();
    compileExpressionList();

    writeSymbol();

    tokenizer->advance();
    writeSymbol();

    indent -= 2;
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "</doStatement>" << std::endl;
    tokenizer->advance();
}

void CompilationEngine::compileWhile() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<whileStatement>" << std::endl;
    indent += 2;

    writeKeyword();

    tokenizer->advance();
    writeSymbol();

    tokenizer->advance();
    compileExpression();

    writeSymbol();

    tokenizer->advance();
    writeSymbol();

    tokenizer->advance();
    compileStatements();

    writeSymbol();

    indent -= 2;
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "</whileStatement>" << std::endl;
    tokenizer->advance();
}

void CompilationEngine::compileIf() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<ifStatement>" << std::endl;
    indent += 2;
    writeKeyword();

    tokenizer->advance();
    writeSymbol();

    tokenizer->advance();
    compileExpression();

    writeSymbol();

    tokenizer->advance();
    writeSymbol();

    tokenizer->advance();
    compileStatements();

    writeSymbol();

    tokenizer->advance();
    if (tokenizer->tokenType() == JackTokens::KEYWORD &&
    tokenizer->getKeyWord() == "else") {
        writeKeyword();

        tokenizer->advance();
        writeSymbol();

        tokenizer->advance();
        compileStatements();

        writeSymbol();
        tokenizer->advance();
    }

    indent -= 2;
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "</ifStatement>" << std::endl;
}

void CompilationEngine::compileLet() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<letStatement>" << std::endl;
    indent += 2;
    writeKeyword();

    tokenizer->advance();
    writeIdentifier();

    tokenizer->advance();
    if (tokenizer->getSymbol() == "[") {
        writeSymbol();
        tokenizer->advance();
        compileExpression();
        writeSymbol();
        tokenizer->advance();
    }

    writeSymbol();

    tokenizer->advance();
    compileExpression();
    writeSymbol();

    indent -= 2;
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "</letStatement>" << std::endl;
    tokenizer->advance();
}

void CompilationEngine::compileExpression() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<expression>" << std::endl;
    indent += 2;

    compileTerm();
//    output << tokenizer->getSymbol() << std::endl;
    while (tokenizer->tokenType() == JackTokens::SYMBOL &&
    std::find(OPS.begin(), OPS.end(), tokenizer->getSymbol()) != OPS.end()) {
        writeSymbol();
        tokenizer->advance();
        compileTerm();
    }
    indent -= 2;
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "</expression>" << std::endl;
}

void CompilationEngine::compileTerm() {
    auto flag = true;
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<term>" << std::endl;
    indent += 2;
    if (tokenizer->tokenType() == JackTokens::INT_CONST) {
        writeIntConstant();
    } else if (tokenizer->tokenType() == JackTokens::STRING_CONST) {
        writeStringConstant();
    } else if (tokenizer->tokenType() == JackTokens::KEYWORD) {
        writeKeyword();
    } else if (tokenizer->tokenType() == JackTokens::IDENTIFIER) {
        writeIdentifier();

        tokenizer->advance();
        flag = false;
        if (tokenizer->getSymbol() == "[") {
            flag = true;
            writeSymbol();
            tokenizer->advance();
            compileExpression();
            writeSymbol();
        } else if (tokenizer->getSymbol() == ".") {
            flag = true;
            writeSymbol();
            tokenizer->advance();
            writeIdentifier();
            tokenizer->advance();
            writeSymbol();
            tokenizer->advance();
            compileExpressionList();
            writeSymbol();
        } else if (tokenizer->getSymbol() ==  "(") {
            flag = true;
            writeSymbol();
            tokenizer->advance();
            compileExpressionList();
            writeSymbol();
        }
    } else if (tokenizer->getSymbol() == "(") {
        writeSymbol();
        tokenizer->advance();
        compileExpression();
        writeSymbol();
    } else if (tokenizer->getSymbol() == "~" || tokenizer->getSymbol() == "-") {
        writeSymbol();
        tokenizer->advance();
        compileTerm();
        flag = false;
    }

    if (flag) {
        tokenizer->advance();
    }

    indent -= 2;
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "</term>" << std::endl;
}

void CompilationEngine::compileExpressionList() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<expressionList>" << std::endl;
    indent += 2;

    if (tokenizer->tokenType() != JackTokens::SYMBOL &&
    tokenizer->getSymbol() != ")") {
        compileExpression();
        while (tokenizer->tokenType() == JackTokens::SYMBOL &&
        tokenizer->getSymbol() == ",") {
            writeSymbol();
            tokenizer->advance();
            compileExpression();
        }
    }
    if (tokenizer->getSymbol() == "(") {
        compileExpression();
        while (tokenizer->tokenType() == JackTokens::SYMBOL &&
               tokenizer->getSymbol() == ",") {
            writeSymbol();
            tokenizer->advance();
            compileExpression();
        }
    }
    indent -= 2;
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "</expressionList>" << std::endl;
}

void CompilationEngine::compileParameterList() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<parameterList>" << std::endl;
    indent += 2;

    while (tokenizer->tokenType() != JackTokens::SYMBOL) {
        if (tokenizer->tokenType() == JackTokens::KEYWORD) {
            writeKeyword();
        } else if (tokenizer->tokenType() == JackTokens::IDENTIFIER) {
            writeIdentifier();
        }
        tokenizer->advance();
        writeIdentifier();
        tokenizer->advance();
        if (tokenizer->getSymbol() == ",") {
            writeSymbol();
            tokenizer->advance();
        }
    }
    indent -= 2;
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "</parameterList>" << std::endl;
}

void CompilationEngine::writeKeyword() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<keyword> " << tokenizer->getKeyWord() << " </keyword>" << std::endl;
}

void CompilationEngine::writeIdentifier() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<identifier> " << tokenizer->getIdentifier() << " </identifier>" << std::endl;
}

void CompilationEngine::writeSymbol() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<symbol> " + tokenizer->getSymbol() + " </symbol>" << std::endl;
}

void CompilationEngine::writeIntConstant() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<integerConstant> " + tokenizer->getIntVal() + " </integerConstant>" << std::endl;
}

void CompilationEngine::writeStringConstant() {
    for (int i = 0; i < indent; ++i) {
        output <<  " ";
    }
    output <<  "<stringConstant> " + tokenizer->getStringVal() + "  </stringConstant>" << std::endl;
}