#include <iostream>
#include <memory>
#include "JackTokenizer.h"

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "Error: Please provide a .jack file or directory." << std::endl;
        return -1;
    }

    auto inputFileName = argv[1];

    auto tokenizer = std::make_shared<JackTokenizer>(inputFileName);
    std::stringstream ss{};
    auto dirString = JackTokenizer::splitString(inputFileName, "/");
    for (int i = 0; i < dirString.size()-1; ++i) {
        ss << dirString[i] + "/";
    }

    auto outputFileName = dirString[dirString.size()-1].find(".jack") != std::string::npos? ss.str() + JackTokenizer::splitString(
            JackTokenizer::getLastInstanceOf(inputFileName, "/"), ".")[0] + "T.xml":
                          ss.str() + dirString[dirString.size()-1] + "/" + JackTokenizer::splitString(
                                  JackTokenizer::getLastInstanceOf(inputFileName, "/"), ".")[0] + "T.xml";

    std::ofstream out{outputFileName};
    out << "<tokens>" << std::endl;
    while (tokenizer->hasMoreTokens()) {
        tokenizer->advance();
        auto token = "";
        std::string val;
        switch (tokenizer->tokenType()) {
            case JackTokens::KEYWORD:
                token = "keyword";
                val = tokenizer->getKeyWord();
                break;
            case JackTokens::SYMBOL:
                token = "symbol";
                val = tokenizer->getSymbol();
                break;
            case JackTokens::INT_CONST:
                token = "integerConstant";
                val = tokenizer->getIntVal();
                break;
            case JackTokens::STRING_CONST:
                token = "stringConstant";
                val = tokenizer->getStringVal();
                break;
            case JackTokens::IDENTIFIER:
                token = "identifier";
                val = tokenizer->getIdentifier();
                break;
        }
        out << "<" << token << ">";
        out << " " << val << " ";
        out << "</" << token << ">";
        out << std::endl;
    }
    out << "</tokens>" << std::endl;
    out.close();
    return 0;
}
