#include "JackTokenizer.h"

JackTokenizer::JackTokenizer(const std::string& input_file_path):
tokenTypeVar(JackTokens::KEYWORD), inputFile(std::ifstream{input_file_path}) {
    if (!inputFile.is_open()) {
        std::cout << "Could not open file" << std::endl;
        return;
    }

    std::stringstream ss;
    ss << inputFile.rdbuf();
    this->text = ss.str();
    this->clearAllComments();
    inputFile.close();
}

void JackTokenizer::clearAllComments() {
    std::stringstream ss;
    std::regex_replace(std::ostreambuf_iterator<char>{ss}, this->text.begin(),
                       this->text.end(), COMMENT, "");
    this->text = ss.str();
}

bool JackTokenizer::hasMoreTokens() {
    return std::regex_match(this->text, EMPTY_TEXT_PATTERN) != 1;
}

void JackTokenizer::advance() {
    if (this->hasMoreTokens()) {
        auto matches = regexSearch(KEYWORD_PATTERN);

        if (!matches.empty()) {
            std::stringstream ss;
            std::regex_replace(std::ostreambuf_iterator<char>{ss}, this->text.begin(),
                               this->text.end(), KEYWORD_PATTERN, "");
            this->text = ss.str();
            this->tokenTypeVar = JackTokens::KEYWORD;
            auto keyword = matches[0];
            this->currentToken = keyword;
        } else {
            matches = regexSearch(SYMBOL_PATTERN);
            if (!matches.empty()) {
                std::stringstream ss;
                std::regex_replace(std::ostreambuf_iterator<char>{ss}, this->text.begin(),
                                   this->text.end(), SYMBOL_PATTERN, "");
                this->text = ss.str();
                this->tokenTypeVar = JackTokens::SYMBOL;
                auto symbol = matches[0];

                if (symbol.find('<') != std::string::npos) {
                    this->currentToken = "&lt;";
                } else if (symbol.find('>') != std::string::npos) {
                    this->currentToken = "&gt;";
                } else if (symbol.find('&') != std::string::npos) {
                    this->currentToken = "&amp;";
                } else {
                    this->currentToken = symbol;
                }
            } else {
                matches = regexSearch(DIGIT_PATTERN);
                if (!matches.empty()) {
                    std::stringstream ss;
                    std::regex_replace(std::ostreambuf_iterator<char>{ss}, this->text.begin(),
                                       this->text.end(), DIGIT_PATTERN, "");
                    this->text = ss.str();
                    this->tokenTypeVar = JackTokens::INT_CONST;
                    auto intConst = matches[0];
                    this->currentToken = intConst;
                } else {
                    matches = regexSearch(STRING_PATTERN);
                    if (!matches.empty()) {
                        std::stringstream ss;
                        std::regex_replace(std::ostreambuf_iterator<char>{ss}, this->text.begin(),
                                           this->text.end(), STRING_PATTERN, "");
                        this->text = ss.str();
                        this->tokenTypeVar = JackTokens::STRING_CONST;
                        auto strConst = matches[0].substr(1);
                        this->currentToken = strConst.substr(0, strConst.length()-1);
                    } else {
                        matches = regexSearch(IDENTIFIER_PATTERN);
                        if (!matches.empty()) {
                            std::stringstream ss;
                            std::regex_replace(std::ostreambuf_iterator<char>{ss}, this->text.begin(),
                                               this->text.end(), IDENTIFIER_PATTERN, "");
                            this->text = ss.str();
                            this->tokenTypeVar = JackTokens::IDENTIFIER;
                            auto identifier = matches[0];
                            this->currentToken = identifier;
                        }
                    }
                }
            }
        }
    }
    trim(this->currentToken);
}

JackTokens JackTokenizer::tokenType() {
    return this->tokenTypeVar;
}

std::string JackTokenizer::getKeyWord() {
    if (this->tokenTypeVar == JackTokens::KEYWORD) {
        return this->currentToken;
    }
    return "";
}

std::string JackTokenizer::getSymbol() {
    if (this->tokenTypeVar == JackTokens::SYMBOL) {
        return this->currentToken;
    }
    return "";
}

std::string JackTokenizer::getIdentifier() {
    if (this->tokenTypeVar == JackTokens::IDENTIFIER) {
        return this->currentToken;
    }
    return "";
}

std::string JackTokenizer::getIntVal() {
    if (this->tokenTypeVar == JackTokens::INT_CONST) {
        return this->currentToken;
    }
    return "";
}

std::string JackTokenizer::getStringVal() {
    if (this->tokenTypeVar == JackTokens::STRING_CONST) {
        return this->currentToken;
    }
    return "";
}

std::vector<std::string> JackTokenizer::regexSearch(const std::regex& regex) {
    std::vector<std::string> matches;
    auto matchesBegin = std::sregex_iterator{this->text.begin(), this->text.end(), regex};
    auto matchesEnd = std::sregex_iterator{};
    for (auto i = matchesBegin; i != matchesEnd; ++i) {
        matches.push_back((*i).str());
    }
    return matches;
}

