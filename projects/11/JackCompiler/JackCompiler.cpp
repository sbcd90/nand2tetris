#include "CompilationEngine.h"
#include <filesystem>

namespace fs = std::filesystem;

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "Error: Please provide a .jack file." << std::endl;
        return -1;
    }

    auto inputFileName = argv[1];

    const fs::path inputPath{inputFileName};
    std::error_code ec;

    if (fs::is_directory(inputPath, ec)) {
        auto jackFiles = std::vector<std::string>{};
        for (const auto &path: fs::directory_iterator(inputPath)) {
            if (path.path().string().find(".jack") != std::string::npos) {
                jackFiles.emplace_back(path.path().string());
            }
        }

        for (auto &jackFile: jackFiles) {
            auto outputFileName = JackTokenizer::splitString(jackFile, ".")[0] + ".vm";
            auto compilationEngine = std::make_shared<CompilationEngine>(jackFile, outputFileName);
            compilationEngine->compileClass();
        }
    }
    if (ec) {
        std::cerr << "Error in is_directory: " << ec.message();
    }

    if (fs::is_regular_file(inputPath, ec)) {
        auto outputFileName = JackTokenizer::splitString(inputFileName, ".")[0] + ".vm";
        auto compilationEngine = std::make_shared<CompilationEngine>(inputFileName, outputFileName);
        compilationEngine->compileClass();
    }
    if (ec) {
        std::cerr << "Error in is_regular_file: " << ec.message();
    }
    return 0;
}