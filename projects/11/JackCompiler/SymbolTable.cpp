#include "SymbolTable.h"

SymbolTable::SymbolTable(): classTable(std::unordered_map<std::string, std::vector<std::string>>{}),
subRoutineTable(std::unordered_map<std::string, std::vector<std::string>>{}), indices(
        std::unordered_map<std::string, int>{
                {"field", 0},
                {"static", 0},
                {"local", 0},
                {"argument", 0}
        }) {
}

void SymbolTable::startSubroutine() {
    subRoutineTable = std::unordered_map<std::string, std::vector<std::string>>{};
    indices["local"] = 0;
    indices["argument"] = 0;
}

void SymbolTable::define(const std::string &name, const std::string &type, const std::string &kind) {
    auto kinds = std::vector<std::string>{};
    kinds.emplace_back("field");
    kinds.emplace_back("static");
    kinds.emplace_back("local");
    kinds.emplace_back("argument");
    if (std::find(kinds.begin(), kinds.end(), kind) != kinds.end()) {
        if (kind == "field" || kind == "static") {
            auto entry = std::vector<std::string>{};
            entry.emplace_back(type);
            entry.emplace_back(kind);
            entry.emplace_back(std::to_string(indices[kind]));

            classTable[name] = entry;
        } else if (kind == "local" || kind == "argument") {
            auto entry = std::vector<std::string>{};
            entry.emplace_back(type);
            entry.emplace_back(kind);
            entry.emplace_back(std::to_string(indices[kind]));

            subRoutineTable[name] = entry;
        }
        ++indices[kind];
    }
}

int SymbolTable::varCount(const std::string &kind) {
    return indices[kind];
}

std::string SymbolTable::kindOf(const std::string &name) {
    if (subRoutineTable.find(name) != subRoutineTable.end()) {
        return subRoutineTable[name][1];
    } else if (classTable.find(name) != classTable.end()) {
        return classTable[name][1];
    }
    return "";
}

std::string SymbolTable::typeOf(const std::string &name) {
    if (subRoutineTable.find(name) != subRoutineTable.end()) {
        return subRoutineTable[name][0];
    } else if (classTable.find(name) != classTable.end()) {
        return classTable[name][0];
    }
    return "";
}

int SymbolTable::indexOf(const std::string &name) {
    if (subRoutineTable.find(name) != subRoutineTable.end()) {
        return std::stoi(subRoutineTable[name][2]);
    } else if (classTable.find(name) != classTable.end()) {
        return std::stoi(classTable[name][2]);
    }
    return -1;
}

