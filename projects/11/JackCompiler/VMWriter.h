#pragma once
#ifndef VM_WRITER_H_
#define VM_WRITER_H_

#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <unordered_map>

class VMWriter {
    std::unordered_map<std::string, std::string> arithDict;
    std::ofstream output;
public:
    VMWriter(const std::string &outputFile);
    void writePush(std::string segment, std::string index);
    void writePop(std::string segment, std::string index);
    void writeArithmetics(const std::string &command);
    void writeLabel(const std::string &label);
    void writeGoto(const std::string &label);
    void writeIf(const std::string &label);
    void writeCall(const std::string &funcName, int nArgs);
    void writeFunc(const std::string &funcName, int localNum);
    void writeReturn();
    void close();
};

#endif // VM_WRITER_H_