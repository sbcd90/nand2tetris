#pragma once
#ifndef COMPILATION_ENGINE_H_
#define COMPILATION_ENGINE_H_

#include "JackTokenizer.h"
#include "SymbolTable.h"
#include "VMWriter.h"
#include <memory>
#include <unordered_map>
#include <vector>

class CompilationEngine {
    typedef void (CompilationEngine::*JackClassFunction)(bool);
    typedef void (CompilationEngine::*JackStatementFunction)();

    std::shared_ptr<JackTokenizer> tokenizer;
    std::shared_ptr<SymbolTable> symbolTable;
    std::shared_ptr<VMWriter> vmWriter;
    std::unordered_map<std::string, CompilationEngine::JackClassFunction> jackClassMap;
    std::unordered_map<std::string, CompilationEngine::JackStatementFunction> jackStatementMap;
    std::unordered_map<JackTokens, JackTokenizer::JackTokensFunction> jackTokenMap;
    bool multiTermExpression;
    int nArgs;
    int nVars;
    int ifLabels;
    int whileLabels;
    std::unordered_map<std::string, std::string> OPS;
    std::string className;
    std::string funcName;
    bool voidMethod;
protected:
    bool checkTypes(const std::vector<JackTokens> &expectedTypes, const std::string &message);
    bool checkTokens(const std::vector<std::string> &expectedTokens, const std::string &token, const std::string &message);
    void process(const std::vector<JackTokens> &expectedTypes, const std::vector<std::string> &expectedTokens = std::vector<std::string>{},
                 const std::string &kind = "", const std::string &varType = "", bool advance = true);
    void compileCall(bool advance, const std::string &classname, const std::string &objectName);
public:
    CompilationEngine(const std::string &inputFilePath, const std::string &outputPath);
    void compileClass();
    void compileClassVarDec(bool advance);
    void compileSubroutineDec(bool advance);
    void compileParameterList(const std::string &subroutineType);
    void compileSubroutineBody(const std::string& subroutineType);
    void compileVarDec();
    void compileStatements();
    void compileLet();
    void compileIf();
    void compileWhile();
    void compileDo();
    void compileReturn();
    void compileExpression(bool advance = true);
    void compileTerm(bool advance = true);
    void compileExpressionList();
};

#endif // COMPILATION_ENGINE_H_