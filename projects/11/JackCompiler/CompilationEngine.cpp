#include "CompilationEngine.h"

CompilationEngine::CompilationEngine(const std::string &inputFilePath, const std::string &outputPath):
tokenizer(std::make_shared<JackTokenizer>(inputFilePath)), symbolTable(std::make_shared<SymbolTable>()),
vmWriter(std::make_shared<VMWriter>(outputPath)), jackClassMap(std::unordered_map<std::string, CompilationEngine::JackClassFunction>{
        {"field", &CompilationEngine::compileClassVarDec},
        {"static", &CompilationEngine::compileClassVarDec},
        {"constructor", &CompilationEngine::compileSubroutineDec},
        {"function", &CompilationEngine::compileSubroutineDec},
        {"method", &CompilationEngine::compileSubroutineDec}
}), jackStatementMap(std::unordered_map<std::string, CompilationEngine::JackStatementFunction>{
        {"let", &CompilationEngine::compileLet},
        {"if", &CompilationEngine::compileIf},
        {"while", &CompilationEngine::compileWhile},
        {"do", &CompilationEngine::compileDo},
        {"return", &CompilationEngine::compileReturn}
}), jackTokenMap(std::unordered_map<JackTokens, JackTokenizer::JackTokensFunction>{
        {JackTokens::SYMBOL, &JackTokenizer::getSymbol},
        {JackTokens::KEYWORD, &JackTokenizer::getKeyWord},
        {JackTokens::IDENTIFIER, &JackTokenizer::getIdentifier},
        {JackTokens::INT_CONST, &JackTokenizer::getIntVal},
        {JackTokens::STRING_CONST, &JackTokenizer::getStringVal}
}), OPS(std::unordered_map<std::string, std::string>{
        {"+", "ADD"},
        {"-", "SUB"},
        {"&", "AND"},
        {"|", "OR"},
        {"<", "LT"},
        {">", "GT"},
        {"=", "EQ"}
}), multiTermExpression(false), nArgs(0), voidMethod(false) {
}

void CompilationEngine::compileClass() {
    auto expectedTypes = std::vector<JackTokens>{};
    expectedTypes.emplace_back(JackTokens::KEYWORD);
    auto expectedTokens = std::vector<std::string>{};
    expectedTokens.emplace_back("class");

    process(expectedTypes, expectedTokens);

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::IDENTIFIER);
    process(expectedTypes, std::vector<std::string>{}, "class");
    className = tokenizer->getIdentifier();

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back("{");
    process(expectedTypes, expectedTokens);
    tokenizer->advance();

    while (!(tokenizer->tokenType() == JackTokens::SYMBOL && tokenizer->getSymbol() == "}")) {
        expectedTypes.clear();
        expectedTypes.emplace_back(JackTokens::KEYWORD);

        auto classKeys = std::vector<std::string>{};
        for (auto & itr : jackClassMap) {
            classKeys.emplace_back(itr.first);
        }

        process(expectedTypes, classKeys, "", "", false);
        auto classMapItr = jackClassMap[tokenizer->getKeyWord()];
        (this->*classMapItr)(false);
    }

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back("}");
    process(expectedTypes, expectedTokens, "", "", false);
}

void CompilationEngine::compileClassVarDec(bool advance) {
    auto expectedTypes = std::vector<JackTokens>{};
    expectedTypes.emplace_back(JackTokens::KEYWORD);

    auto expectedTokens = std::vector<std::string>{};
    expectedTokens.emplace_back("field");
    expectedTokens.emplace_back("static");
    process(expectedTypes, expectedTokens, "", "", advance);

    auto kind = tokenizer->getKeyWord();

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::KEYWORD);
    expectedTypes.emplace_back(JackTokens::IDENTIFIER);
    expectedTokens.clear();
    expectedTokens.emplace_back("void");
    expectedTokens.emplace_back("int");
    expectedTokens.emplace_back("char");
    expectedTokens.emplace_back("boolean");
    process(expectedTypes, expectedTokens);
    auto varType = tokenizer->getCurrentToken();

    while (!(tokenizer->tokenType() == JackTokens::SYMBOL && tokenizer->getSymbol() == ";")) {
        expectedTypes.clear();
        expectedTypes.emplace_back(JackTokens::IDENTIFIER);
        process(expectedTypes, std::vector<std::string>{}, kind, varType);
        expectedTypes.clear();
        expectedTypes.emplace_back(JackTokens::SYMBOL);
        expectedTokens.clear();
        expectedTokens.emplace_back(",");
        expectedTokens.emplace_back(";");
        process(expectedTypes, expectedTokens);
    }
    tokenizer->advance();
}

void CompilationEngine::compileSubroutineDec(bool advance) {
    ifLabels = 0;
    whileLabels = 0;

    auto expectedTypes = std::vector<JackTokens>{};
    expectedTypes.emplace_back(JackTokens::KEYWORD);
    auto expectedTokens = std::vector<std::string>{};
    expectedTokens.emplace_back("constructor");
    expectedTokens.emplace_back("function");
    expectedTokens.emplace_back("method");
    process(expectedTypes, expectedTokens, "", "", false);
    auto subRoutineType = tokenizer->getKeyWord();

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::KEYWORD);
    expectedTypes.emplace_back(JackTokens::IDENTIFIER);
    expectedTokens.clear();
    expectedTokens.emplace_back("void");
    expectedTokens.emplace_back("int");
    expectedTokens.emplace_back("char");
    expectedTokens.emplace_back("boolean");
    process(expectedTypes, expectedTokens);

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::IDENTIFIER);
    process(expectedTypes, std::vector<std::string>{}, "subroutine");
    funcName = className + "." + tokenizer->getIdentifier();

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back("(");
    process(expectedTypes, expectedTokens);

    compileParameterList(subRoutineType);
    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back(")");
    process(expectedTypes, expectedTokens, "", "", false);

    if (tokenizer->getKeyWord() == "void") {
        voidMethod = true;
    } else {
        voidMethod = false;
    }

    compileSubroutineBody(subRoutineType);
}

void CompilationEngine::compileParameterList(const std::string &subroutineType) {
    symbolTable->startSubroutine();
    if (subroutineType == "method") {
        symbolTable->define("this", className, "argument");
    }
    tokenizer->advance();
    auto nParams = 0;
    if (tokenizer->getSymbol() != ")") {
        auto expectedTypes = std::vector<JackTokens>();
        expectedTypes.emplace_back(JackTokens::KEYWORD);
        expectedTypes.emplace_back(JackTokens::IDENTIFIER);
        auto expectedTokens = std::vector<std::string>();
        expectedTokens.emplace_back("int");
        expectedTokens.emplace_back("char");
        expectedTokens.emplace_back("boolean");
        process(expectedTypes, expectedTokens, "", "", false);

        expectedTypes.clear();
        expectedTypes.emplace_back(JackTokens::IDENTIFIER);
        process(expectedTypes, std::vector<std::string>{}, "argument");

        nParams = 1;
        tokenizer->advance();
        while (!(tokenizer->tokenType() == JackTokens::SYMBOL && tokenizer->getSymbol() == ")")) {
            expectedTypes.clear();
            expectedTypes.emplace_back(JackTokens::SYMBOL);
            expectedTokens.clear();
            expectedTokens.emplace_back(",");
            process(expectedTypes, expectedTokens, "", "", false);

            expectedTypes.clear();
            expectedTypes.emplace_back(JackTokens::KEYWORD);
            expectedTypes.emplace_back(JackTokens::IDENTIFIER);
            expectedTokens.clear();
            expectedTokens.emplace_back("int");
            expectedTokens.emplace_back("char");
            expectedTokens.emplace_back("boolean");
            process(expectedTypes, expectedTokens, "object");

            expectedTypes.clear();
            expectedTypes.emplace_back(JackTokens::IDENTIFIER);
            process(expectedTypes, std::vector<std::string>{}, "argument");

            ++nParams;
            tokenizer->advance();
        }
    }
}

void CompilationEngine::compileSubroutineBody(const std::string& subroutineType) {
    auto expectedTypes = std::vector<JackTokens>{};
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    auto expectedTokens = std::vector<std::string>{};
    expectedTokens.emplace_back("{");
    process(expectedTypes, expectedTokens);

    tokenizer->advance();
    nVars = 0;
    while (tokenizer->tokenType() == JackTokens::KEYWORD && tokenizer->getKeyWord() == "var") {
        compileVarDec();
        tokenizer->advance();
    }

    vmWriter->writeFunc(funcName, nVars);

    if (subroutineType == "constructor") {
        vmWriter->writePush("constant", std::to_string(symbolTable->varCount("field")));
        vmWriter->writeCall("Memory.alloc", 1);
        vmWriter->writePop("pointer", "0");
    } else if (subroutineType == "method") {
        vmWriter->writePush("argument", "0");
        vmWriter->writePop("pointer", "0");
    }

    compileStatements();

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back("}");
    process(expectedTypes, expectedTokens, "", "", false);
    tokenizer->advance();
}

void CompilationEngine::compileVarDec() {
    auto expectedTypes = std::vector<JackTokens>{};
    expectedTypes.emplace_back(JackTokens::KEYWORD);
    auto expectedTokens = std::vector<std::string>{};
    expectedTokens.emplace_back("var");
    process(expectedTypes, expectedTokens, "", "", false);

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::KEYWORD);
    expectedTypes.emplace_back(JackTokens::IDENTIFIER);
    expectedTokens.clear();
    expectedTokens.emplace_back("int");
    expectedTokens.emplace_back("char");
    expectedTokens.emplace_back("boolean");
    process(expectedTypes, expectedTokens, "class");

    while (true) {
        expectedTypes.clear();
        expectedTypes.emplace_back(JackTokens::IDENTIFIER);
        process(expectedTypes, std::vector<std::string>{}, "local");

        ++nVars;
        expectedTypes.clear();
        expectedTypes.emplace_back(JackTokens::SYMBOL);
        expectedTokens.clear();
        expectedTokens.emplace_back(",");
        expectedTokens.emplace_back(";");
        process(expectedTypes, expectedTokens);

        if (tokenizer->tokenType() == JackTokens::SYMBOL && tokenizer->getSymbol() == ";") {
            break;
        }
    }
}

void CompilationEngine::compileStatements() {
    while (!(tokenizer->tokenType() == JackTokens::SYMBOL && tokenizer->getSymbol() == "}")) {
        auto expectedTypes = std::vector<JackTokens>{};
        expectedTypes.emplace_back(JackTokens::KEYWORD);
        auto expectedTokens = std::vector<std::string>{};
        expectedTokens.emplace_back("let");
        expectedTokens.emplace_back("if");
        expectedTokens.emplace_back("while");
        expectedTokens.emplace_back("do");
        expectedTokens.emplace_back("return");
        process(expectedTypes, expectedTokens, "", "", false);

        auto stmtItr = jackStatementMap[tokenizer->getKeyWord()];
        (this->*stmtItr)();
    }
}

void CompilationEngine::compileLet() {
    auto expectedTypes = std::vector<JackTokens>{};
    expectedTypes.emplace_back(JackTokens::IDENTIFIER);
    auto expectedTokens = std::vector<std::string>{};
    process(expectedTypes, expectedTokens);

    auto var = tokenizer->getIdentifier();

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back("=");
    expectedTokens.emplace_back("[");
    process(expectedTypes, expectedTokens);

    if (tokenizer->getSymbol() == "[") {
        vmWriter->writePush(symbolTable->kindOf(var), std::to_string(symbolTable->indexOf(var)));
        compileExpression();
        vmWriter->writeArithmetics("ADD");

        expectedTypes.clear();
        expectedTypes.emplace_back(JackTokens::SYMBOL);
        expectedTokens.clear();
        expectedTokens.emplace_back("]");
        process(expectedTypes, expectedTokens, "", "", false);

        expectedTypes.clear();
        expectedTypes.emplace_back(JackTokens::SYMBOL);
        expectedTokens.clear();
        expectedTokens.emplace_back("=");
        process(expectedTypes, expectedTokens);

        compileExpression();
        vmWriter->writePop("temp", "0");
        vmWriter->writePop("pointer", "1");
        vmWriter->writePush("temp", "0");
        vmWriter->writePop("that", "0");
    } else {
        compileExpression();
        vmWriter->writePop(symbolTable->kindOf(var), std::to_string(symbolTable->indexOf(var)));
    }

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back(";");
    process(expectedTypes, expectedTokens, "", "", false);

    tokenizer->advance();
}

void CompilationEngine::compileIf() {
    auto expectedTypes = std::vector<JackTokens>{};
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    auto expectedTokens = std::vector<std::string>{};
    expectedTokens.emplace_back("(");
    process(expectedTypes, expectedTokens, "", "", true);

    compileExpression();
    vmWriter->writeArithmetics("NOT");

    auto label1 = "IF_TRUE" + std::to_string(ifLabels);
    vmWriter->writeIf(label1);
    auto label2 = "IF_FALSE" + std::to_string(ifLabels);
    ++ifLabels;

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back(")");
    process(expectedTypes, expectedTokens, "", "", false);

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back("{");
    process(expectedTypes, expectedTokens);

    tokenizer->advance();
    compileStatements();

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back("}");
    process(expectedTypes, expectedTokens, "", "", false);

    tokenizer->advance();
    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::KEYWORD);
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    checkTypes(expectedTypes, "Failure");

    if (tokenizer->tokenType() == JackTokens::KEYWORD && tokenizer->getKeyWord() == "else") {
        vmWriter->writeGoto(label2);
        vmWriter->writeLabel(label1);

        expectedTypes.clear();
        expectedTypes.emplace_back(JackTokens::SYMBOL);
        expectedTokens.clear();
        expectedTokens.emplace_back("{");
        process(expectedTypes, expectedTokens);

        tokenizer->advance();
        compileStatements();
        vmWriter->writeLabel(label2);

        expectedTypes.clear();
        expectedTypes.emplace_back(JackTokens::SYMBOL);
        expectedTokens.clear();
        expectedTokens.emplace_back("}");
        process(expectedTypes, expectedTokens, "", "", false);
        tokenizer->advance();
    } else {
        vmWriter->writeLabel(label1);
    }
}

void CompilationEngine::compileWhile() {
    auto expectedTypes = std::vector<JackTokens>{};
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    auto expectedTokens = std::vector<std::string>{};
    expectedTokens.emplace_back("(");
    process(expectedTypes, expectedTokens);

    auto label1 = "WHILE_EXP" + std::to_string(whileLabels);
    vmWriter->writeLabel(label1);
    auto label2 = "WHILE_END" + std::to_string(whileLabels);
    ++whileLabels;

    compileExpression();
    vmWriter->writeArithmetics("NOT");
    vmWriter->writeIf(label2);

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back(")");
    process(expectedTypes, expectedTokens, "", "", false);

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back("{");
    process(expectedTypes, expectedTokens);

    tokenizer->advance();
    compileStatements();
    vmWriter->writeGoto(label1);
    vmWriter->writeLabel(label2);

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back("}");
    process(expectedTypes, expectedTokens, "", "", false);

    tokenizer->advance();
}

void CompilationEngine::compileDo() {
    auto expectedTypes = std::vector<JackTokens>{};
    expectedTypes.emplace_back(JackTokens::KEYWORD);
    auto expectedTokens = std::vector<std::string>{};
    expectedTokens.emplace_back("do");
    process(expectedTypes, expectedTokens, "", "", false);

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::IDENTIFIER);
    expectedTokens.clear();
    expectedTokens.emplace_back("class or subroutine");
    process(expectedTypes, expectedTokens, "subroutine");

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back("(");
    expectedTokens.emplace_back(".");
    process(expectedTypes, expectedTokens);

    using namespace std::string_literals;
    auto objectName = ""s;
    auto classname = ""s;

    if (tokenizer->getSymbol() == ".") {
        objectName = tokenizer->getIdentifier();
        classname = symbolTable->typeOf(objectName);
        if (classname.empty()) {
            classname = objectName;
        } else {
            vmWriter->writePush(symbolTable->kindOf(objectName), std::to_string(symbolTable->indexOf(objectName)));
        }
    } else {
        classname = this->className;
    }

    compileCall(true, classname, objectName);
    vmWriter->writePop("temp", "0");

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back(";");
    process(expectedTypes, expectedTokens);

    tokenizer->advance();
}

void CompilationEngine::compileCall(bool advance, const std::string &classname, const std::string &objectName) {
    nArgs = 0;
    using namespace std::string_literals;
    auto funcname = ""s;
    if (tokenizer->getSymbol() == "(") {
        funcname = tokenizer->getPreviousToken();
        vmWriter->writePush("pointer", "0");
        compileExpressionList();
        ++nArgs;
    } else if (tokenizer->getSymbol() == ".") {
        auto expectedTypes = std::vector<JackTokens>{};
        expectedTypes.emplace_back(JackTokens::IDENTIFIER);
        auto expectedTokens = std::vector<std::string>{};
        process(expectedTypes, expectedTokens, "subroutine");
        funcname = tokenizer->getIdentifier();

        expectedTypes.clear();
        expectedTypes.emplace_back(JackTokens::SYMBOL);
        expectedTokens.clear();
        expectedTokens.emplace_back("(");
        process(expectedTypes, expectedTokens);
        compileExpressionList();
    }

    auto expectedTypes = std::vector<JackTokens>{};
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    auto expectedTokens = std::vector<std::string>{};
    expectedTokens.emplace_back(")");
    process(expectedTypes, expectedTokens, "", "", false);

    if (symbolTable->classTable.find(objectName) != symbolTable->classTable.end() ||
    symbolTable->subRoutineTable.find(objectName) != symbolTable->subRoutineTable.end()) {
        ++nArgs;
    }
    vmWriter->writeCall(classname + "." + funcname, nArgs);
}

void CompilationEngine::compileReturn() {
    auto expectedTypes = std::vector<JackTokens>{};
    expectedTypes.emplace_back(JackTokens::KEYWORD);
    auto expectedTokens = std::vector<std::string>{};
    expectedTokens.emplace_back("return");
    process(expectedTypes, expectedTokens, "", "", false);

    tokenizer->advance();
    if (tokenizer->getKeyWord() == "this") {
        vmWriter->writePush("pointer", "0");
        tokenizer->advance();
    } else if (!(tokenizer->tokenType() == JackTokens::SYMBOL && tokenizer->getSymbol() == ";")) {
        compileExpression(false);
    } else {
        vmWriter->writePush("constant", "0");
    }
    vmWriter->writeReturn();

    expectedTypes.clear();
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    expectedTokens.clear();
    expectedTokens.emplace_back(";");
    process(expectedTypes, expectedTokens, "", "", false);

    tokenizer->advance();
}

void CompilationEngine::compileExpression(bool advance) {
    multiTermExpression = false;
    compileTerm(advance);

    auto symbols = std::vector<std::string>{};
    symbols.emplace_back("+");
    symbols.emplace_back("-");
    symbols.emplace_back("*");
    symbols.emplace_back("/");
    symbols.emplace_back("&");
    symbols.emplace_back("|");
    symbols.emplace_back("<");
    symbols.emplace_back(">");
    symbols.emplace_back("=");

    if (tokenizer->tokenType() == JackTokens::SYMBOL && std::find(symbols.begin(), symbols.end(), tokenizer->getSymbol()) != symbols.end()) {
        while (true) {
            multiTermExpression = true;

            auto expectedTypes = std::vector<JackTokens>{};
            expectedTypes.emplace_back(JackTokens::SYMBOL);
            process(expectedTypes, symbols, "", "", false);
            auto op = tokenizer->getSymbol();
            compileTerm();

            auto symbolSubset = std::unordered_map<std::string, std::string>{
                    {"*", "Math.multiply"},
                    {"/", "Math.divide"}
            };
            if (symbolSubset.find(op) != symbolSubset.end()) {
                vmWriter->writeCall(symbolSubset[op], 2);
            } else {
                vmWriter->writeArithmetics(OPS[op]);
            }
            if (std::find(symbols.begin(), symbols.end(), tokenizer->getSymbol()) == symbols.end()) {
                break;
            }
        }
    }
}

void CompilationEngine::compileTerm(bool advance) {
    auto expectedTypes = std::vector<JackTokens>{};
    expectedTypes.emplace_back(JackTokens::INT_CONST);
    expectedTypes.emplace_back(JackTokens::STRING_CONST);
    expectedTypes.emplace_back(JackTokens::KEYWORD);
    expectedTypes.emplace_back(JackTokens::IDENTIFIER);
    expectedTypes.emplace_back(JackTokens::SYMBOL);
    auto expectedTokens = std::vector<std::string>{};
    expectedTokens.emplace_back("true");
    expectedTokens.emplace_back("false");
    expectedTokens.emplace_back("null");
    expectedTokens.emplace_back("this");
    expectedTokens.emplace_back("(");
    expectedTokens.emplace_back("-");
    expectedTokens.emplace_back("~");
    process(expectedTypes, expectedTokens, "", "", advance);

    if (tokenizer->tokenType() == JackTokens::INT_CONST) {
        vmWriter->writePush("constant", tokenizer->getIntVal());
    }
    if (tokenizer->tokenType() == JackTokens::STRING_CONST) {
        auto stringVal = tokenizer->getStringVal();
        vmWriter->writePush("constant", std::to_string(stringVal.length()));
        vmWriter->writeCall("String.new", 1);
        for (char i : stringVal) {
            vmWriter->writePush("constant", std::to_string(i));
            vmWriter->writeCall("String.appendChar", 2);
        }
    }
    if (tokenizer->tokenType() == JackTokens::KEYWORD) {
        auto keywords = std::vector<std::string>{};
        keywords.emplace_back("false");
        keywords.emplace_back("null");
        if (std::find(keywords.begin(), keywords.end(), tokenizer->getKeyWord()) != keywords.end()) {
            vmWriter->writePush("constant", "0");
        }
        if (tokenizer->getKeyWord() == "true") {
            vmWriter->writePush("constant", "1");
            vmWriter->writeArithmetics("NEG");
        }
        if (tokenizer->getKeyWord() == "this") {
            vmWriter->writePush("pointer", "0");
        }
        tokenizer->advance();
    }

    JackTokens previousTokenType;
    if (tokenizer->tokenType() == JackTokens::SYMBOL) {
        previousTokenType = tokenizer->tokenType();
        auto symbols = std::unordered_map<std::string, std::string>{
                {"-", "NEG"},
                {"~", "NOT"}
        };

        if (symbols.find(tokenizer->getSymbol()) != symbols.end()) {
            auto sym = tokenizer->getSymbol();
            compileTerm();
            vmWriter->writeArithmetics(symbols[sym]);
        } else if (tokenizer->getSymbol() == "(") {
            compileExpression();

            expectedTypes.clear();
            expectedTypes.emplace_back(JackTokens::SYMBOL);
            expectedTokens.clear();
            expectedTokens.emplace_back(")");
            process(expectedTypes, expectedTokens, "", "", false);
            tokenizer->advance();
        }
    } else {
        previousTokenType = tokenizer->tokenType();
        tokenizer->advance();
    }

    auto symbols = std::vector<std::string>{};
    symbols.emplace_back("[");
    symbols.emplace_back("(");
    symbols.emplace_back(".");
    if (tokenizer->tokenType() == JackTokens::SYMBOL && std::find(symbols.begin(), symbols.end(), tokenizer->getSymbol()) != symbols.end()) {
        if (tokenizer->getSymbol() == "[") {
            vmWriter->writePush(symbolTable->kindOf(tokenizer->getIdentifier()), std::to_string(symbolTable->indexOf(tokenizer->getIdentifier())));

            expectedTypes.clear();
            expectedTypes.emplace_back(JackTokens::SYMBOL);
            expectedTokens.clear();
            expectedTokens.emplace_back("[");
            process(expectedTypes, expectedTokens, "", "", false);

            compileExpression();
            vmWriter->writeArithmetics("ADD");
            vmWriter->writePop("pointer", "1");
            vmWriter->writePush("that", "0");

            expectedTypes.clear();
            expectedTypes.emplace_back(JackTokens::SYMBOL);
            expectedTokens.clear();
            expectedTokens.emplace_back("]");
            process(expectedTypes, expectedTokens, "", "", false);
        } else {
            using namespace std::string_literals;
            auto objectName = ""s;
            auto classname = ""s;

            if (tokenizer->getSymbol() == ".") {
                objectName = tokenizer->getIdentifier();
                classname = symbolTable->typeOf(objectName);
                if (symbolTable->classTable.find(objectName) != symbolTable->classTable.end() ||
                symbolTable->subRoutineTable.find(objectName) != symbolTable->subRoutineTable.end()) {
                    vmWriter->writePush(symbolTable->kindOf(objectName), std::to_string(symbolTable->indexOf(objectName)));
                }
                if (classname.empty()) {
                    classname = objectName;
                }
            } else {
                objectName = "";
                classname = className;
            }
            compileCall(true, classname, objectName);
        }
        tokenizer->advance();
    } else if (previousTokenType == JackTokens::IDENTIFIER) {
        vmWriter->writePush(symbolTable->kindOf(tokenizer->getPreviousToken()),
                            std::to_string(symbolTable->indexOf(tokenizer->getPreviousToken())));
    }
}

void CompilationEngine::compileExpressionList() {
    tokenizer->advance();
    if (tokenizer->getSymbol() != ")") {
        compileExpression(false);
        nArgs = 1;
        while (tokenizer->getSymbol() == ",") {
            auto expectedTypes = std::vector<JackTokens>{};
            expectedTypes.emplace_back(JackTokens::SYMBOL);
            auto expectedTokens = std::vector<std::string>{};
            expectedTokens.emplace_back(",");
            process(expectedTypes, expectedTokens, "", "", false);

            compileExpression();
            ++nArgs;
        }
    }
}

bool CompilationEngine::checkTypes(const std::vector<JackTokens> &expectedTypes, const std::string &message) {
    if (std::find(expectedTypes.begin(), expectedTypes.end(), tokenizer->tokenType()) != expectedTypes.end()) {
        return true;
    }
    std::cout << tokenizer->getCurrentToken() << std::endl;
//    throw message;
    return false;
}

bool CompilationEngine::checkTokens(const std::vector<std::string> &expectedTokens, const std::string &token, const std::string &message) {
    if (std::find(expectedTokens.begin(), expectedTokens.end(), token) == expectedTokens.end()) {
        return false;
//        throw message;
    }
    return true;
}

void CompilationEngine::process(const std::vector<JackTokens> &expectedTypes,
                                const std::vector<std::string> &expectedTokens, const std::string &kind,
                                const std::string &varType, bool advance) {
    if (advance) {
        tokenizer->advance();
    }
    checkTypes(expectedTypes, "Exception");
    auto tokenMapItr = jackTokenMap[tokenizer->tokenType()];
    auto token = ((tokenizer.get())->*tokenMapItr)();

    if (tokenizer->tokenType() == JackTokens::SYMBOL || tokenizer->tokenType() == JackTokens::KEYWORD) {
        checkTokens(expectedTokens, token, "Exception");
    } else if (tokenizer->tokenType() == JackTokens::IDENTIFIER) {
        if (symbolTable->kindOf(tokenizer->getIdentifier()).empty()) {
            symbolTable->define(tokenizer->getIdentifier(), tokenizer->getPreviousToken(), kind);
        }
    }
}