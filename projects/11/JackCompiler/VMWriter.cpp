#include "VMWriter.h"

VMWriter::VMWriter(const std::string &outputFile): output(std::ofstream{outputFile}), arithDict(
        std::unordered_map<std::string, std::string>{
                {"ADD", "add"},
                {"SUB", "sub"},
                {"NEG", "neg"},
                {"EQ", "eq"},
                {"GT", "gt"},
                {"LT", "lt"},
                {"AND", "and"},
                {"OR", "or"},
                {"NOT", "not"}
        }) {
}

void VMWriter::writePush(std::string segment, std::string index) {
    if (index.empty()) {
        segment = "constant";
        index = "0";
    }
    if (segment == "field") {
        segment = "this";
    }
    output << "push " << segment << " " << index << std::endl;
}

void VMWriter::writePop(std::string segment, std::string index) {
    if (segment == "field") {
        segment = "this";
    }
    output << "pop " << segment << " " << index << std::endl;
}

void VMWriter::writeArithmetics(const std::string &command) {
    output << arithDict[command] << std::endl;
}

void VMWriter::writeLabel(const std::string &label) {
    output << "label" << " " << label << std::endl;
}

void VMWriter::writeGoto(const std::string &label) {
    output << "goto" << " " << label << std::endl;
}

void VMWriter::writeIf(const std::string &label) {
    output << "if-goto" << " " << label << std::endl;
}

void VMWriter::writeCall(const std::string &funcName, int nArgs) {

    output << "call" << " " << funcName << " " << std::to_string(nArgs) << std::endl;
}

void VMWriter::writeFunc(const std::string &funcName, int localNum) {
    output << "function" << " " << funcName << " " << std::to_string(localNum) << std::endl;
}

void VMWriter::writeReturn() {
    output << "return" << std::endl;
}

void VMWriter::close() {
    output.close();
}