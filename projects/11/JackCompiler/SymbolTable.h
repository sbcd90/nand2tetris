#pragma once
#ifndef SYMBOL_TABLE_H_
#define SYMBOL_TABLE_H_

#include <string>
#include <unordered_map>
#include <vector>
#include <algorithm>

class SymbolTable {
    std::unordered_map<std::string, int> indices;
public:
    std::unordered_map<std::string, std::vector<std::string>> classTable;
    std::unordered_map<std::string, std::vector<std::string>> subRoutineTable;

    SymbolTable();
    void startSubroutine();
    void define(const std::string &name, const std::string &type, const std::string &kind);
    int varCount(const std::string &kind);
    std::string kindOf(const std::string &name);
    std::string typeOf(const std::string &name);
    int indexOf(const std::string &name);

};

#endif // SYMBOL_TABLE_H_