#ifndef SYMBOL_TABLE_H_
#define SYMBOL_TABLE_H_

#include <string>
#include <unordered_map>

class SymbolTable {
    std::unordered_map<std::string, int> table;
public:
    SymbolTable();
    void addEntry(const std::string &symbol, const int &value);
    bool contains(const std::string &symbol);
    int getAddress(const std::string &symbol);
};

#endif // SYMBOL_TABLE_H_