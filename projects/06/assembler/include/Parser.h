#pragma once
#ifndef PARSER_H_
#define PARSER_H_

#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <locale>

class Parser {
    std::string cmd;
    int current;
    std::vector<std::string> commands;
public:
    enum CommandType {
        A,
        L,
        C
    };

    Parser(const std::string &fileName);
    bool hasMoreCommands();
    void advance();
    void restart();
    CommandType commandType();
    std::string symbol();
    std::string dest();
    std::string comp();
    std::string jump();

    static inline void ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
            return !std::isspace(static_cast<char>(ch), std::locale("en_US.UTF-8"));
        }));
    }

    static inline void rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
            return !std::isspace(static_cast<char>(ch), std::locale("en_US.UTF-8"));
        }).base(), s.end());
    }

    static inline void trim(std::string &s) {
        rtrim(s);
        ltrim(s);
    }

    static inline std::vector<std::string> splitString(const std::string &s, const std::string &delimiter = " ") {
        auto splits = std::vector<std::string>{};
        auto start = 0;
        auto end = s.find(delimiter);
        while (end != std::string::npos) {
            splits.push_back(s.substr(start, end - start));
            start = end + delimiter.length();
            end = s.find(delimiter, start);
        }
        splits.push_back(s.substr(start, end - start));
        return splits;
    }
};

#endif // PARSER_H_