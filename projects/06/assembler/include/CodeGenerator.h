#ifndef CODE_GENERATOR_H_
#define CODE_GENERATOR_H_
#include <string>
#include <unordered_map>

class CodeGenerator {
    std::unordered_map<std::string, std::string> dTable;
    std::unordered_map<std::string, std::string> cTable;
    std::unordered_map<std::string, std::string> jTable;
public:
    CodeGenerator();
    std::string dest(const std::string &mnemonic);
    std::string comp(const std::string &mnemonic);
    std::string jump(const std::string &mnemonic);
};

#endif // CODE_GENERATOR_H_