cmake_minimum_required(VERSION 3.19)
project(assembler)

set(CMAKE_CXX_STANDARD 14)

add_executable(assembler Assembler.cpp Parser.cpp CodeGenerator.cpp SymbolTable.cpp)
target_include_directories(assembler PUBLIC include/)