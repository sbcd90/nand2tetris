#include <Parser.h>
#include <CodeGenerator.h>
#include <SymbolTable.h>
#include <bitset>

bool isNumber(const std::string &s){
    for (char const &ch : s) {
        if (std::isdigit(ch, std::locale("en_US.UTF-8")) == 0)
            return false;
    }
    return true;
}

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "Error: Please provide a Hack assembly file." << std::endl;
        return -1;
    }

    auto inputFileName = argv[1];
    auto parser = std::make_shared<Parser>(inputFileName);
    auto symbols = std::make_shared<SymbolTable>();

    int counter = 0;
    while (parser->hasMoreCommands()) {
        parser->advance();
        if (parser->commandType() == Parser::CommandType::L) {
            symbols->addEntry(parser->symbol(), counter);
        } else {
            counter += 1;
        }
    }
    parser->restart();

    auto codeGen = std::make_shared<CodeGenerator>();

    auto outputFileName = parser->splitString(inputFileName, ".asm")[0] + ".hack";
    auto file = std::ofstream{outputFileName};

    int variable = 16;
    while (parser->hasMoreCommands()) {
        parser->advance();

        if (parser->commandType() == Parser::CommandType::A) {
            auto num = 0;
            auto symbol = parser->symbol();

            if (isNumber(symbol)) {
                num = std::stoi(symbol);
            } else if (symbols->contains(symbol)) {
                num = symbols->getAddress(symbol);
            } else {
                num = variable;
                symbols->addEntry(symbol, num);
                variable += 1;
            }
            file << std::bitset<16>(num).to_string() << std::endl;
        } else if (parser->commandType() == Parser::CommandType::C) {
            auto comp = codeGen->comp(parser->comp());
            auto dest = codeGen->dest(parser->dest());
            auto jmp = codeGen->jump(parser->jump());
            file << ("111" + comp + dest + jmp) << std::endl;
        }
    }
    file.close();
    return 0;
}