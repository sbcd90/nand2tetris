#include "Parser.h"

Parser::Parser(const std::string &fileName) {
    this->cmd = "";
    this->current = -1;
    this->commands = std::vector<std::string>{};

    auto file = std::ifstream{fileName};

    if (!file.is_open()) {
        std::cout << "Could not open file" << std::endl;
        return;
    }

    while (!file.fail() && !file.eof()) {
        std::string s;
        std::getline(file, s);

        auto line = splitString(s, "//")[0];
        trim(line);

        if (!line.empty()) {
            this->commands.push_back(line);
        }
    }
    file.close();
}

bool Parser::hasMoreCommands() {
    return this->current + 1 < this->commands.size();
}

void Parser::advance() {
    this->current += 1;
    this->cmd = this->commands[this->current];
}

void Parser::restart() {
    this->cmd = "";
    this->current = -1;
}

Parser::CommandType Parser::commandType() {
    if (this->cmd[0] == '@') {
        return CommandType::A;
    } else if (this->cmd[0] == '(') {
        return CommandType::L;
    } else {
        return CommandType::C;
    }
}

std::string Parser::symbol() {
    if (this->commandType() == CommandType::A) {
        return this->cmd.substr(1);
    }
    if (this->commandType() == CommandType::L) {
        return this->cmd.substr(1, this->cmd.size() - 2);
    }
    return "";
}

std::string Parser::dest() {
    if (this->commandType() == CommandType::C) {
        if (this->cmd.find("=") != std::string::npos) {
            return splitString(this->cmd, "=")[0];
        }
    }
    return "";
}

std::string Parser::comp() {
    if (this->commandType() == CommandType::C) {
        auto tmp = this->cmd;
        if (tmp.find("=") != std::string::npos) {
            tmp = splitString(tmp, "=")[1];
        }
        return splitString(tmp, ";")[0];
    }
    return "";
}

std::string Parser::jump() {
    if (this->commandType() == CommandType::C) {
        auto tmp = this->cmd;
        if (tmp.find("=") != std::string::npos) {
            tmp = splitString(tmp, "=")[1];
        }
        return splitString(tmp, ";")[1];
    }
    return "";
}