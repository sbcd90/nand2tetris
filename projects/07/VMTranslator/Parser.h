#pragma once
#ifndef PARSER_H_
#define PARSER_H_

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

enum CommandType {
    C_ARITHMETIC,
    C_PUSH,
    C_POP
};

class Parser {
    std::string currentCommand;
    std::string fileName;
    std::vector<std::string> commands;
    int current;
protected:
    static inline void ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
            return !std::isspace(static_cast<char>(ch), std::locale("en_US.UTF-8"));
        }));
    }

    static inline void rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
            return !std::isspace(static_cast<char>(ch), std::locale("en_US.UTF-8"));
        }).base(), s.end());
    }

    static inline void trim(std::string &s) {
        rtrim(s);
        ltrim(s);
    }
public:
    Parser(const std::string &fileName);
    bool hasMoreCommands();
    void advance();
    CommandType commandType();
    std::string arg1();
    int arg2();

    static inline std::vector<std::string> splitString(const std::string &s, const std::string &delimiter = " ") {
        auto splits = std::vector<std::string>{};
        auto start = 0;
        auto end = s.find(delimiter);
        while (end != std::string::npos) {
            splits.push_back(s.substr(start, end - start));
            start = end + delimiter.length();
            end = s.find(delimiter, start);
        }
        splits.push_back(s.substr(start, end - start));
        return splits;
    }

    static inline std::string getLastInstanceOf(const std::string &s, const std::string &delimiter = " ") {
        auto splits = splitString(s, delimiter);
        return splits[splits.size()-1];
    }
};
#endif // PARSER_H_