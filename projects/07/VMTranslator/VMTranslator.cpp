#include "Parser.h"
#include "CodeWriter.h"
#include <memory>

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "Error: Please provide a VM file." << std::endl;
        return -1;
    }

    auto inputFileName = argv[1];
    auto parser = std::make_shared<Parser>(inputFileName);
    auto outputFileName = Parser::splitString(inputFileName, ".")[0] + ".asm";

    auto codeWriter = std::make_shared<CodeWriter>(outputFileName);

    while (parser->hasMoreCommands()) {
        parser->advance();

        auto commandType = parser->commandType();
        if (commandType == C_ARITHMETIC) {
            codeWriter->writeArithmetic(parser->arg1());
        } else if (commandType == C_PUSH || commandType == C_POP) {
            auto arg1 = parser->arg1();
            auto arg2 = parser->arg2();

            codeWriter->writePushPop(commandType, arg1, arg2);
        } else {
            throw "Unsupported Command Type";
        }
    }
    codeWriter->close();
    return 0;
}