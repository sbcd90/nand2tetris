#pragma once
#ifndef CODE_WRITER_H_
#define CODE_WRITER_H_

#include <string>
#include <fstream>
#include <unordered_map>

class CodeWriter {
    std::string fileName;
    std::ofstream file;
    int labelCounter;
    std::unordered_map<std::string, std::string> symbols;
public:
    CodeWriter(const std::string &fileName);
    void writeArithmetic(const std::string &command);
    void writePushPop(CommandType command, const std::string &segment, const int index);
    void close();
};

#endif // CODE_WRITER_H_