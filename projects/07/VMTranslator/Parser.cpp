#include "Parser.h"

Parser::Parser(const std::string &fileName):
currentCommand(""), current(-1), commands(std::vector<std::string>{}), fileName(fileName) {
    auto file = std::ifstream{};
    file.open(this->fileName);

    if (!file.is_open()) {
        std::cout << "Could not open file" << std::endl;
        return;
    }

    while (!file.fail() && !file.eof()) {
        std::string s;
        std::getline(file, s);
        auto line = splitString(s, "//")[0];
        trim(line);

        if (!line.empty()) {
            this->commands.push_back(line);
        }
    }
    file.close();
}

void Parser::advance() {
    this->current += 1;
    this->currentCommand = this->commands[this->current];
}

CommandType Parser::commandType() {
    auto arithMeticCommands = std::vector<std::string>{
            "add", "sub", "neg", "eq", "gt", "lt", "and", "or", "not"};
    auto cmd = splitString(this->currentCommand, " ")[0];

    if (std::find(arithMeticCommands.begin(), arithMeticCommands.end(), cmd) != arithMeticCommands.end()) {
        return C_ARITHMETIC;
    } else if (cmd == "push") {
        return C_PUSH;
    } else if (cmd == "pop") {
        return C_POP;
    } else {
        throw "Unexpected Command Type";
    }
}

bool Parser::hasMoreCommands() {
    return this->current + 1 < this->commands.size();
}

std::string Parser::arg1() {
    if (this->commandType() == C_ARITHMETIC) {
        auto arg = splitString(this->currentCommand, " ")[0];
        return arg;
    } else {
        auto arg = splitString(this->currentCommand, " ")[1];
        return arg;
    }
}

int Parser::arg2() {
    auto arg2 = splitString(this->currentCommand, " ")[2];
    return std::stoi(arg2);
}