#include "Parser.h"
#include "CodeWriter.h"

CodeWriter::CodeWriter(const std::string &fileName):
fileName(fileName), labelCounter(0) {
    this->file = std::ofstream{fileName};
    this->symbols = std::unordered_map<std::string, std::string>{
            {"add", "M=D+M"},
            {"sub", "M=M-D"},
            {"and", "M=D&M"},
            {"or", "M=D|M"},
            {"neg", "M=-M"},
            {"not", "M=!M"},
            {"eq", "D;JEQ"},
            {"gt", "D;JGT"},
            {"lt", "D;JLT"},
            {"local", "@LCL"},
            {"argument", "@ARG"},
            {"this", "@THIS"},
            {"that", "@THAT"},
            {"constant", ""},
            {"static", ""},
            {"pointer", "@3"},
            {"temp", "@5"}
    };
}

void CodeWriter::writeArithmetic(const std::string &command) {
    auto output = std::vector<std::string>{};
    auto binaryCommands = std::vector<std::string>{"add", "sub", "and", "or"};
    auto unaryCommands = std::vector<std::string>{"neg", "not"};
    auto logicalCommands = std::vector<std::string>{"eq", "gt", "lt"};

    if (std::find(binaryCommands.begin(), binaryCommands.end(), command) != binaryCommands.end()) {
        output.emplace_back("@SP");
        output.emplace_back("AM=M-1");
        output.emplace_back("D=M");

        output.emplace_back("@SP");
        output.emplace_back("A=M-1");
        output.emplace_back(this->symbols[command]);
    } else if (std::find(unaryCommands.begin(), unaryCommands.end(), command) != unaryCommands.end()) {
        output.emplace_back("@SP");
        output.emplace_back("A=M-1");
        output.emplace_back(this->symbols[command]);
    } else if (std::find(logicalCommands.begin(), logicalCommands.end(), command) != logicalCommands.end()) {
        auto jmpLabel = "CmpLabel" + std::to_string(this->labelCounter);
        ++this->labelCounter;
        output.emplace_back("@SP");
        output.emplace_back("AM=M-1");
        output.emplace_back("D=M");

        output.emplace_back("@SP");
        output.emplace_back("A=M-1");
        output.emplace_back("D=M-D");
        output.emplace_back("M=-1");

        output.emplace_back("@" + jmpLabel);
        output.emplace_back(this->symbols[command]);
        output.emplace_back("@SP");
        output.emplace_back("A=M-1");
        output.emplace_back("M=0");

        output.emplace_back("(" + jmpLabel + ")");
    } else {
        throw "Unexpected Arithmetic Command";
    }
    output.emplace_back("");

    int size = output.size();
    for (int i = 0; i < size; ++i) {
        file << output[i] << "\n";
    }
}

void CodeWriter::writePushPop(CommandType command, const std::string &segment, const int index) {
    auto output = std::vector<std::string>{};
    auto segments = std::vector<std::string>{"local", "argument", "this", "that", "temp", "pointer"};

    if (command == C_PUSH) {
        if (segment == "constant") {
            output.emplace_back("@" + std::to_string(index));
            output.emplace_back("D=A");
            output.emplace_back("@SP");
            output.emplace_back("AM=M+1");
            output.emplace_back("A=A-1");
            output.emplace_back("M=D");
        } else if (std::find(segments.begin(), segments.end(), segment) != segments.end()) {
            output.emplace_back("@" + std::to_string(index));
            output.emplace_back("D=A");

            if (segment == "temp" || segment == "pointer") {
                output.emplace_back(this->symbols[segment]);
            } else {
                output.emplace_back(this->symbols[segment]);
                output.emplace_back("A=M");
            }

            output.emplace_back("A=D+A");
            output.emplace_back("D=M");

            output.emplace_back("@SP");
            output.emplace_back("A=M");
            output.emplace_back("M=D");

            output.emplace_back("@SP");
            output.emplace_back("M=M+1");
        } else if (segment == "static") {
            output.emplace_back("@" + Parser::getLastInstanceOf(this->fileName, "/") + "." + std::to_string(index));
            output.emplace_back("D=M");

            output.emplace_back("@SP");
            output.emplace_back("A=M");
            output.emplace_back("M=D");

            output.emplace_back("@SP");
            output.emplace_back("M=M+1");
        } else {
            throw "Unexpected Push Segment";
        }
    } else if (command == C_POP) {
        if (segment == "constant") {
            throw "Cannot Pop Constant Segment";
        } else if (std::find(segments.begin(), segments.end(), segment) != segments.end()) {
            output.emplace_back("@" + std::to_string(index));
            output.emplace_back("D=A");

            if (segment == "temp" || segment == "pointer") {
                output.emplace_back(this->symbols[segment]);
            } else {
                output.emplace_back(this->symbols[segment]);
                output.emplace_back("A=M");
            }

            output.emplace_back("D=D+A");
            output.emplace_back("@R13");
            output.emplace_back("M=D");

            output.emplace_back("@SP");
            output.emplace_back("AM=M-1");
            output.emplace_back("D=M");

            output.emplace_back("@R13");
            output.emplace_back("A=M");
            output.emplace_back("M=D");
        } else if (segment == "static") {
            output.emplace_back("@SP");
            output.emplace_back("AM=M-1");
            output.emplace_back("D=M");

            output.emplace_back("@" + Parser::getLastInstanceOf(this->fileName, "/") + "." + std::to_string(index));
            output.emplace_back("M=D");
        } else {
            throw "Unexpected Pop Segment";
        }
    } else {
        throw "Unexpected Command Type";
    }
    output.emplace_back("");

    int size = output.size();
    for (int i = 0; i < size; ++i) {
        file << output[i] << "\n";
    }
}

void CodeWriter::close() {
    this->file.close();
}