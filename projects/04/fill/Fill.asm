// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.
(KBDLOOP)
@SCREEN
D=A
@addr
M=D

@KBD
D=M
@BLACK_LOOP
D;JGT
@WHITE_LOOP
D;JEQ

(BLACK_LOOP)
D=-1
@addr
A=M
M=D

@addr
M=M+1
D=M

@24576
D=D-A
@BLACK_LOOP
D;JLT
@KBDLOOP
D;JGE

(WHITE_LOOP)
D=0
@addr
A=M
M=D

@addr
M=M+1
D=M

@24576
D=D-A
@WHITE_LOOP
D;JLT


@KBDLOOP
0;JMP
