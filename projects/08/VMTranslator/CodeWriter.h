#pragma once
#ifndef CODE_WRITER_H_
#define CODE_WRITER_H_

#include "Parser.h"

class CodeWriter {
    std::string fileName;
    int commandNum;
    std::ofstream file;
    std::string functionName;
    std::vector<std::string> commands;

    void writePushSymbol(const std::string &symbol, bool isConst = false);
public:
    CodeWriter(const std::string& outFileName);
    void writeArithmetic(const std::string &command);
    void writePushPop(CommandType command, const std::string &segment, const int index);
    void close();
    void setFileName(const std::string &vmFileName);
    void writeInit();
    void writeLabel(const std::string &label);
    void writeIf(const std::string &label);
    void writeGoto(const std::string &label);
    void writeFunction(const std::string &functionName, int numVars);
    void writeCall(const std::string &functionName, int numArgs);
    void writeReturn();
    void writeToFile();
};

#endif //CODE_WRITER_H_