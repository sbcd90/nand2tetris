#include "Parser.h"

Parser::Parser(std::string inFileName): inFileName(std::move(inFileName)),
lines(std::vector<std::string>{}), lineNum(-1), currLine("") {
    auto file = std::ifstream{this->inFileName};

    if (!file.is_open()) {
        std::cout << "Could not open file" << std::endl;
        return;
    }

    while (!file.fail() && !file.eof()) {
        std::string s;
        std::getline(file, s);
        auto line = splitString(s, "//")[0];
        trim(line);

        if (!line.empty()) {
            this->lines.push_back(line);
        }
    }
    file.close();
    this->lastLineNum = this->lines.size() - 1;
}

bool Parser::hasMoreCommands() {
    return this->lineNum < this->lastLineNum;
}

void Parser::advance() {
    this->lineNum += 1;
    this->currLine = this->lines[lineNum];
}

CommandType Parser::commandType() {
    if (currLine.find("push") != std::string::npos) {
        return C_PUSH;
    } else if (currLine.find("pop") != std::string::npos) {
        return C_POP;
    } else if (currLine.find("label") != std::string::npos) {
        return C_LABEL;
    } else if (currLine.find("if-goto") != std::string::npos) {
        return C_IF;
    } else if (currLine.find("goto") != std::string::npos) {
        return C_GOTO;
    } else if (currLine.find("function") != std::string::npos) {
        return C_FUNCTION;
    } else if (currLine.find("call") != std::string::npos) {
        return C_CALL;
    } else if (currLine.find("return") != std::string::npos) {
        return C_RETURN;
    } else {
        return C_ARITHMETIC;
    }
}

std::string Parser::arg1() {
    if (this->commandType() == C_ARITHMETIC) {
        return this->currLine;
    } else {
        return splitString(this->currLine, " ")[1];
    }
}

int Parser::arg2() {
    return std::stoi(splitString(this->currLine, " ")[2]);
}