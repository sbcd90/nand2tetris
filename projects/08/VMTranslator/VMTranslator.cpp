#include "Parser.h"
#include "CodeWriter.h"
#include <filesystem>
#include <iostream>
#include <sstream>

namespace fs = std::filesystem;

void writeCode(const std::shared_ptr<CodeWriter> &codeWriter, const std::string &vmFilePath) {
    auto parser = std::make_shared<Parser>(vmFilePath);
    codeWriter->setFileName(vmFilePath);

    while (parser->hasMoreCommands()) {
        parser->advance();
        auto cmdType = parser->commandType();

        if (cmdType == C_ARITHMETIC) {
            codeWriter->writeArithmetic(parser->arg1());
        } else if (cmdType == C_PUSH) {
            codeWriter->writePushPop(CommandType::C_PUSH, parser->arg1(), parser->arg2());
        } else if (cmdType == C_POP) {
            codeWriter->writePushPop(CommandType::C_POP, parser->arg1(), parser->arg2());
        } else if (cmdType == C_LABEL) {
            codeWriter->writeLabel(parser->arg1());
        } else if (cmdType == C_IF) {
            codeWriter->writeIf(parser->arg1());
        } else if (cmdType == C_GOTO) {
            codeWriter->writeGoto(parser->arg1());
        } else if (cmdType == C_CALL) {
            codeWriter->writeCall(parser->arg1(), parser->arg2());
        } else if (cmdType == C_FUNCTION) {
            codeWriter->writeFunction(parser->arg1(), parser->arg2());
        } else {
            codeWriter->writeReturn();
        }
    }
}

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "Error: Please provide a VM file." << std::endl;
        return -1;
    }

    auto inputFileName = argv[1];

    std::stringstream ss{};
    auto dirString = Parser::splitString(inputFileName, "/");
    for (int i = 0; i < dirString.size()-1; ++i) {
        ss << dirString[i] + "/";
    }

    auto outputFileName = dirString[dirString.size()-1].find(".vm") != std::string::npos? ss.str() + Parser::splitString(
            Parser::getLastInstanceOf(inputFileName, "/"), ".")[0] + ".asm":
                    ss.str() + dirString[dirString.size()-1] + "/" + Parser::splitString(
                            Parser::getLastInstanceOf(inputFileName, "/"), ".")[0] + ".asm";
    auto codeWriter = std::make_shared<CodeWriter>(outputFileName);

    const fs::path inputPath{inputFileName};
    std::error_code ec;

    if (fs::is_directory(inputPath, ec)) {
        auto vmFiles = std::vector<std::string>{};
        for (const auto &path: fs::directory_iterator(inputPath)) {
            if (path.path().string().find(".vm") != std::string::npos) {
                vmFiles.emplace_back(path.path().string());
            }
        }

        codeWriter->writeInit();
        for (auto &vmFile: vmFiles) {
            writeCode(codeWriter, vmFile);
        }
        codeWriter->writeToFile();
    }
    if (ec) {
        std::cerr << "Error in is_directory: " << ec.message();
    }

    if (fs::is_regular_file(inputPath, ec)) {
        writeCode(codeWriter, inputFileName);
        codeWriter->writeToFile();
    }
    if (ec) {
        std::cerr << "Error in is_regular_file: " << ec.message();
    }
    codeWriter->close();
    return 0;
}