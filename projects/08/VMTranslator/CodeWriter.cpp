#include "CodeWriter.h"

CodeWriter::CodeWriter(const std::string& outFileName): file(std::ofstream{outFileName}),
commandNum(0), functionName(""), fileName(""), commands(std::vector<std::string>{}) {}

void CodeWriter::close() {
    this->file.close();
}

void CodeWriter::setFileName(const std::string &vmFileName) {
    this->fileName = vmFileName;
}

void CodeWriter::writeArithmetic(const std::string &command) {
    this->commandNum += 1;

    commands.emplace_back("@SP");
    commands.emplace_back("A=M-1");

    if (command == "neg") {
        commands.emplace_back("M=-M");
        return;
    }
    if (command == "not") {
        commands.emplace_back("M=!M");
        return;
    }
    commands.emplace_back("D=M");
    commands.emplace_back("A=A-1");

    if (command == "add") {
        commands.emplace_back("M=D+M");
    } else if (command == "sub") {
        commands.emplace_back("M=M-D");
    } else if (command == "and") {
        commands.emplace_back("M=D&M");
    } else if (command == "or") {
        commands.emplace_back("M=D|M");
    } else {
        commands.emplace_back("D=M-D");
        commands.emplace_back("@IF_TRUE" + std::to_string(commandNum));

        if (command == "eq") {
            commands.emplace_back("D;JEQ");
        } else if (command == "gt") {
            commands.emplace_back("D;JGT");
        } else if (command == "lt") {
            commands.emplace_back("D;JLT");
        }

        commands.emplace_back("@SP");
        commands.emplace_back("A=M-1");
        commands.emplace_back("A=A-1");
        commands.emplace_back("M=0");

        commands.emplace_back("@IF_END" + std::to_string(commandNum));
        commands.emplace_back("0;JMP");

        commands.emplace_back("(IF_TRUE" + std::to_string(commandNum) + ")");
        commands.emplace_back("@SP");
        commands.emplace_back("A=M-1");
        commands.emplace_back("A=A-1");
        commands.emplace_back("M=0");
        commands.emplace_back("M=!M");

        commands.emplace_back("(IF_END" + std::to_string(commandNum) + ")");
    }

    commands.emplace_back("@SP");
    commands.emplace_back("M=M-1");
}

void CodeWriter::writeInit() {
    commands.emplace_back("@256");
    commands.emplace_back("D=A");
    commands.emplace_back("@SP");
    commands.emplace_back("M=D");

    this->writeCall("Sys.init", 0);
}

void CodeWriter::writePushPop(CommandType command, const std::string &segment, const int index) {
    this->commandNum += 1;

    if (command == C_PUSH) {
        if (segment == "constant") {
            commands.emplace_back("@" + std::to_string(index));
            commands.emplace_back("D=A");
        } else {
            if (segment == "static") {
                commands.emplace_back("@" + Parser::splitString(Parser::getLastInstanceOf(fileName, "/"), ".")[0] + "." + std::to_string(index));
            } else {
                commands.emplace_back("@" + std::to_string(index));
                commands.emplace_back("D=A");

                if (segment == "pointer" || segment == "temp") {
                    if (segment == "pointer") {
                        commands.emplace_back("@3");
                    } else {
                        commands.emplace_back("@5");
                    }
                    commands.emplace_back("A=D+A");
                } else {
                    if (segment == "argument") {
                        commands.emplace_back("@ARG");
                    } else if (segment == "local") {
                        commands.emplace_back("@LCL");
                    } else if (segment == "this") {
                        commands.emplace_back("@THIS");
                    } else if (segment == "that") {
                        commands.emplace_back("@THAT");
                    }

                    commands.emplace_back("A=D+M");
                }
            }
            commands.emplace_back("D=M");
        }

        commands.emplace_back("@SP");
        commands.emplace_back("A=M");
        commands.emplace_back("M=D");

        commands.emplace_back("@SP");
        commands.emplace_back("M=M+1");
    } else {
        if (segment == "static") {
            commands.emplace_back("@SP");
            commands.emplace_back("A=M-1");
            commands.emplace_back("D=M");
            commands.emplace_back("@" + Parser::splitString(Parser::getLastInstanceOf(fileName, "/"), ".")[0] + "." + std::to_string(index));
            commands.emplace_back("M=D");
        } else {
            commands.emplace_back("@" + std::to_string(index));
            commands.emplace_back("D=A");

            if (segment == "pointer" || segment == "temp") {
                if (segment == "pointer") {
                    commands.emplace_back("@3");
                } else {
                    commands.emplace_back("@5");
                }
                commands.emplace_back("D=D+A");
            } else {
                if (segment == "argument") {
                    commands.emplace_back("@ARG");
                } else if (segment == "local") {
                    commands.emplace_back("@LCL");
                } else if (segment == "this") {
                    commands.emplace_back("@THIS");
                } else if (segment == "that") {
                    commands.emplace_back("@THAT");
                }
                commands.emplace_back("D=D+M");
            }
            commands.emplace_back("@R13");
            commands.emplace_back("M=D");

            commands.emplace_back("@SP");
            commands.emplace_back("A=M-1");
            commands.emplace_back("D=M");

            commands.emplace_back("@R13");
            commands.emplace_back("A=M");
            commands.emplace_back("M=D");
        }
        commands.emplace_back("@SP");
        commands.emplace_back("M=M-1");
    }
}

void CodeWriter::writeLabel(const std::string &label) {
    this->commandNum += 1;
    commands.emplace_back("(" + this->functionName + "$" + label + ")");
}

void CodeWriter::writeGoto(const std::string &label) {
    this->commandNum += 1;
    commands.emplace_back("@" + this->functionName + "$" + label);
    commands.emplace_back("0;JMP");
}

void CodeWriter::writeIf(const std::string &label) {
    this->commandNum += 1;

    commands.emplace_back("@SP");
    commands.emplace_back("A=M-1");
    commands.emplace_back("D=M");

    commands.emplace_back("@SP");
    commands.emplace_back("M=M-1");

    commands.emplace_back("@" + this->functionName + "$" + label);
    commands.emplace_back("D;JNE");
}

void CodeWriter::writeCall(const std::string &functionName, int numArgs) {
    this->commandNum += 1;

    auto returnAddress = "RETURN_A" + std::to_string(this->commandNum);
    this->writePushSymbol(returnAddress, true);

    this->writePushSymbol("LCL");
    this->writePushSymbol("ARG");
    this->writePushSymbol("THIS");
    this->writePushSymbol("THAT");

    commands.emplace_back("@" + std::to_string((numArgs + 5)));
    commands.emplace_back("D=A");

    commands.emplace_back("@SP");
    commands.emplace_back("D=M-D");

    commands.emplace_back("@ARG");
    commands.emplace_back("M=D");

    commands.emplace_back("@SP");
    commands.emplace_back("D=M");
    commands.emplace_back("@LCL");
    commands.emplace_back("M=D");

    commands.emplace_back("@" + functionName);
    commands.emplace_back("0;JMP");
    commands.emplace_back("(" + returnAddress + ")");
}

void CodeWriter::writePushSymbol(const std::string &symbol, bool isConst) {
    commands.emplace_back("@" + symbol);
    if (isConst) {
        commands.emplace_back("D=A");
    } else {
        commands.emplace_back("D=M");
    }

    commands.emplace_back("@SP");
    commands.emplace_back("A=M");
    commands.emplace_back("M=D");

    commands.emplace_back("@SP");
    commands.emplace_back("M=M+1");
}

void CodeWriter::writeFunction(const std::string &functionName, int numVars) {
    this->commandNum += 1;

    this->functionName = functionName;
    commands.emplace_back("(" + functionName + ")");

    for (int i = 0; i < numVars; ++i) {
        this->writePushSymbol("0", true);
    }
}

void CodeWriter::writeReturn() {
    this->commandNum += 1;

    commands.emplace_back("@LCL");
    commands.emplace_back("D=M");

    commands.emplace_back("@R14");
    commands.emplace_back("M=D");

    commands.emplace_back("@5");
    commands.emplace_back("A=D-A");
    commands.emplace_back("D=M");

    commands.emplace_back("@R15");
    commands.emplace_back("M=D");

    commands.emplace_back("@SP");
    commands.emplace_back("A=M-1");
    commands.emplace_back("D=M");

    commands.emplace_back("@ARG");
    commands.emplace_back("A=M");
    commands.emplace_back("M=D");
    commands.emplace_back("D=A");
    commands.emplace_back("@SP");
    commands.emplace_back("M=D+1");

    auto symbolList = std::vector<std::string>{"THAT", "THIS", "ARG", "LCL"};
    for (int i = 0; i < symbolList.size(); ++i) {
        commands.emplace_back("@R14");
        commands.emplace_back("AM=M-1");
        commands.emplace_back("D=M");

        commands.emplace_back("@" + symbolList[i]);
        commands.emplace_back("M=D");
    }

    commands.emplace_back("@R15");
    commands.emplace_back("A=M");
    commands.emplace_back("0;JMP");
}

void CodeWriter::writeToFile() {
    int size = commands.size();
    for (int i = 0; i < size; ++i) {
        file << commands[i] << "\n";
    }
}